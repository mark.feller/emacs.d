(use-package undo-tree
  :disabled
  :delight undo-tree-mode
  :config (global-undo-tree-mode t))

(provide 'module-undo-tree)
